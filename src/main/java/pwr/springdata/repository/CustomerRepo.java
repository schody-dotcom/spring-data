package pwr.springdata.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pwr.springdata.service.Customer;

@Repository
public interface CustomerRepo extends CrudRepository<Customer, Long> {

}
