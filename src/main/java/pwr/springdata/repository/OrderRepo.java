package pwr.springdata.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pwr.springdata.service.Order;

@Repository
public interface OrderRepo extends CrudRepository<Order, Long> {
}
