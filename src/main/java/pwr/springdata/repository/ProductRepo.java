package pwr.springdata.repository;



import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pwr.springdata.service.Product;

@Repository
public interface ProductRepo extends CrudRepository<Product, Long> {



}
