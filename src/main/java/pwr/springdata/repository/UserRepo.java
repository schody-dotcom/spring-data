package pwr.springdata.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pwr.springdata.service.UserDto;

public interface UserRepo extends CrudRepository<UserDto, Long> {

}
