package pwr.springdata.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import pwr.springdata.repository.CustomerRepo;
import pwr.springdata.repository.OrderRepo;
import pwr.springdata.repository.ProductRepo;
import pwr.springdata.service.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {
    private ProductRepo productRepository;
    private OrderRepo orderRepository;
    private CustomerRepo customerRepository;
    private UserRepo userRepository;

    public DbMockData() {
    }

    @Autowired
    public DbMockData(ProductRepo productRepository, OrderRepo orderRepository, CustomerRepo customerRepository, UserRepo userRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.userRepository = userRepository;
    }


    @EventListener
            (ApplicationReadyEvent.class)
    public void fill() {
        Product product = new Product("zgrzewka wody", 11.49f, true);
        Product product1 = new Product("płyn do naczyń", 4.99f, false);
        Product product2 = new Product("rowerek", 27f, true);
        Product product3 = new Product("szynka", 4.99f, true);
        Customer customer = new Customer("Grażyna Stołeczna", "Lublin");
        Customer customer1 = new Customer("Michał Staszek", "Warszawa");
        Set<Product> products = new HashSet<>() {
            {
                add(product);
                add(product1);
            }
        };
        Set<Product> products1 = new HashSet<>() {
            {
                add(product2);
                add(product3);
            }
        };
        Order order = new Order(customer, products, LocalDateTime.now(), "in progress");
        Order order1 = new Order(customer1, products1, LocalDateTime.now(), "in progress");


        UserDto user = new UserDtoBuilder(new User("karol", "karol1", "ROLE_CUSTOMER")).getUserDto();
        UserDto admin = new UserDtoBuilder(new User("admin", "admin1", "ROLE_ADMIN")).getUserDto();

        productRepository.save(product);
        productRepository.save(product1);
        productRepository.save(product2);
        productRepository.save(product3);
        customerRepository.save(customer);
        customerRepository.save(customer1);
        orderRepository.save(order);
        orderRepository.save(order1);
        userRepository.save(user);
        userRepository.save(admin);
    }


}
