package pwr.springdata.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;


import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    private PasswordEncoder passwordEncoder = new PasswordEncoderConfig().passwordEncoder();


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.jdbcAuthentication()
                .usersByUsernameQuery("SELECT u.name, u.password_hash, 1 FROM USER_DTO u WHERE u.name=?")
                .authoritiesByUsernameQuery("SELECT u.name, u.role, 1 FROM USER_DTO u WHERE u.name=?")
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
                http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/api/product").permitAll()
                .antMatchers(HttpMethod.GET, "/api/product/all").hasAnyRole("ADMIN", "CUSTOMER")
                .antMatchers(HttpMethod.GET, "/api/order").hasAnyRole("ADMIN", "CUSTOMER")
                .antMatchers(HttpMethod.GET, "/api/order/all").hasAnyRole("ADMIN", "CUSTOMER")
                .antMatchers(HttpMethod.POST, "/api/order").hasAnyRole("ADMIN", "CUSTOMER")
                .antMatchers(HttpMethod.GET, "/api/customer").hasRole("CUSTOMER")
                .antMatchers(HttpMethod.GET, "/api/customer/all").hasRole("CUSTOMER")
                .antMatchers(HttpMethod.POST, "/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/api/admin/customer").hasRole("ADMIN")


                .antMatchers("https://io-labs3.ew.r.appspot.com/").hasRole("ADMIN")
                .antMatchers("/console/**").hasRole("ADMIN")
                .anyRequest().authenticated().and().httpBasic()
                .and().csrf().disable()
                .headers().frameOptions().disable();


    }
}