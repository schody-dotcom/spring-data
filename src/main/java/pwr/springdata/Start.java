package pwr.springdata;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pwr.springdata.repository.ProductRepo;

@Component
public class Start {

    private ProductRepo productRepo;

    @Autowired
    public Start(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }


}
