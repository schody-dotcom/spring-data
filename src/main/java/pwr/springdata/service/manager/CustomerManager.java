package pwr.springdata.service.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pwr.springdata.repository.CustomerRepo;
import pwr.springdata.service.Customer;

import java.util.Optional;

@Service
public class CustomerManager {

    private CustomerRepo customerRepo;

    @Autowired
    public CustomerManager(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;

    }

    public  Optional<Customer> findById(Long id){
        return customerRepo.findById(id);
    }

    public Iterable<Customer> findAll(){
        return customerRepo.findAll();
    }

    public Customer save(Customer Customer){
        return customerRepo.save(Customer);
    }

    public void deleteById(Long id){
        customerRepo.deleteById(id);
    }



}
