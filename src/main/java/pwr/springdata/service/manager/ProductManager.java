package pwr.springdata.service.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pwr.springdata.repository.ProductRepo;
import pwr.springdata.service.Product;

import java.util.Optional;

@Service
public class ProductManager {

    private ProductRepo productRepo;

    @Autowired
    public ProductManager(ProductRepo productRepo) {
        this.productRepo = productRepo;

    }

    public  Optional<Product> findById(Long id){
        return productRepo.findById(id);
    }

    public Iterable<Product> findAll(){
        return productRepo.findAll();
    }

    public Product save(Product Product){
        return productRepo.save(Product);
    }

    public void deleteById(Long id){
        productRepo.deleteById(id);
    }



}
