package pwr.springdata.service;

import org.springframework.security.crypto.password.PasswordEncoder;
import pwr.springdata.security.PasswordEncoderConfig;


public class UserDtoBuilder {

    UserDto userDto;

    public UserDtoBuilder(User user) {
        this.userDto = new UserDto();
        this.userDto.setName(user.getName());
        PasswordEncoder passwordEncoder =  new PasswordEncoderConfig().passwordEncoder();
        this.userDto.setPasswordHash(passwordEncoder.encode(user.getPassword()));
        this.userDto.setRole(user.getRole());
    }

    public UserDto getUserDto() {
        return userDto;
    }

}
