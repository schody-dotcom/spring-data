package pwr.springdata.controller.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pwr.springdata.service.Customer;
import pwr.springdata.service.Order;
import pwr.springdata.service.manager.OrderManager;


import java.util.Optional;

@RestController
@RequestMapping
public class OrderApi {

    private OrderManager orders;

    @Autowired
    public OrderApi(OrderManager orders) {
        this.orders = orders;
    }

    @GetMapping("api/order/all")
    public Iterable<Order> getAllOrders() {
        return orders.findAll();
    }

    @GetMapping("api/order")
    public Optional<Order> getOrderById(@RequestParam Long index) {
        return orders.findById(index);
    }

    @PostMapping("api/order")
    public Order addOrder(@RequestBody Order order ){
        return  orders.save(order);
    }

    @PutMapping("/api/admin/order")
    public Order updateOrder(@RequestBody Order order ){
        return  orders.save(order);
    }

    @DeleteMapping("api/admin/order")
    public void deleteOrder(@RequestBody Long index){
        orders.deleteById(index);
    }

    @PatchMapping("/api/admin/order")
    public Order partlyUpdateCustomer(@RequestBody Order order ){
        return  orders.save(order);
    }


}
