package pwr.springdata.controller.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pwr.springdata.service.Customer;
import pwr.springdata.service.Product;
import pwr.springdata.service.manager.ProductManager;


import java.util.Optional;

@RestController
@RequestMapping
public class ProductApi {

    private ProductManager products;

    @Autowired
    public ProductApi(ProductManager products) {
        this.products = products;
    }

    @GetMapping("/api/product/all")
    public Iterable<Product> getAllProducts() {
        return products.findAll();
    }

    @GetMapping("/api/product")
    public Optional<Product> getProductById(@RequestParam Long index) {
        return products.findById(index);
    }

    @PostMapping("/api/admin/product")
    public Product addProduct(@RequestBody Product product ){
        return  products.save(product);
    }

    @PutMapping("/api/admin/product")
    public Product updateProduct(@RequestBody Product product ){
        return  products.save(product);
    }

    @DeleteMapping("/api/admin/product")
    public void deleteProduct(@RequestBody Long index){
        products.deleteById(index);
    }

    @PatchMapping("/api/admin/product")
    public Product partlyUpdateProduct(@RequestBody Product product ){
        return  products.save(product);
    }


}
